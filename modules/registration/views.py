import hashlib
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.auth.models import User
from models import *
from modules.helpers import *
from django.http import HttpResponseRedirect

class LoginView(View):

    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, 'templates/registration/login.html')

    def post(self, request, *args, **kwargs):
        data = request.POST
        if 'username' in data and 'password' in data:
            username = data['username']
            password = data['password']
            return HttpResponse('login posted')
        else:
            return render(request, 'templates/registration/login.html')

class SimpleView(View):

    def dispatch(self, request, *args, **kwargs):
        return super(SimpleView, self).dispatch(request, *args, **kwargs)

    def get(self, request, homework_id, *args, **kwargs):
        try:
            homework = Homework.objects.filter(id=homework_id)[0]
            title = homework.title
            homework_id = homework.id
            print homework_id
            print title
            return render(request, 'templates/registration/submit.html', {'title':title,'homework_id':homework_id})
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def post(self, request, *args, **kwargs):
        try:
            data = request.POST
            attributes = clean_data(data)
            answer = attributes['answer']
            homework_id = attributes['homework_id']
            user = User.objects.filter(id=request.user.id)[0]
            options = {}
            options['user_id'] = user.id
            options['answer'] = answer
            options['homework_id'] = homework_id
            submission = Submission(**options)
            submission.save()
            return HttpResponseRedirect("/home")
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def check_superuser(self, request):
        user = User.objects.filter(id=request.user.id)[0]
        if not user.is_superuser:
            return False
        return True

    def get_homework_by_user_id(self, request, user_id):
        try:
            homeworks = get_objects(HomeworkUser.objects.filter(user_id=user_id))
            return HttpResponse(json.dumps(homeworks))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def get_submissions_by_homework_id(self, request, homework_id):
        try:
            if not self.check_superuser(request):
                return HttpResponse(json.dumps({'error':'Not Logged In a Teacher'}))
            print homework_id
            submissions = get_objects(Submission.objects.filter(homework_id=homework_id))
            return HttpResponse(json.dumps(submissions))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def get_submissions_by_homework_id_and_user_id(self, request, homework_id, user_id):
        try:
            if not self.check_superuser(request):
                return HttpResponse(json.dumps({'error':'Not Logged In a Teacher'}))
            submissions = get_objects(Submission.objects.filter(homework_id=homework_id , user_id=user_id))
            return HttpResponse(json.dumps(submissions))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def get_submissions(self, request):
        try:
            if not self.check_superuser(request):
                return HttpResponse(json.dumps({'error':'Not Logged In a Teacher'}))
            x = Submission.objects.filter()
            submissions = get_objects(Submission.objects.filter())
            return HttpResponse(json.dumps(submissions))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def get_submissions(self, request):
        try:
            if not self.check_superuser(request):
                return HttpResponse(json.dumps({'error':'Not Logged In a Teacher'}))
            submissions = get_objects(Submission.objects.filter())
            return HttpResponse(json.dumps(submissions))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))

    def get_homeworks(self, request):
        try:
            if not self.check_superuser(request):
                return HttpResponse(json.dumps({'error':'Not Logged In a Teacher'}))
            submissions = get_objects(Homework.objects.filter())
            return HttpResponse(json.dumps(submissions))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)

    def get_users(self, request):
        try:
            if not self.check_superuser(request):
                return HttpResponse(json.dumps({'error':'Not Logged In a Teacher'}))
            users = User.objects.filter()
            normalized_users = []
            for user in users:
                u = {
                    'id':user.id,
                    'usename':user.username
                }
                normalized_users.append(u)
            return HttpResponse(json.dumps(normalized_users))
        except Exception as e:
            error = {}
            error['status'] = 400
            error['error'] = str(e)
            return HttpResponse(json.dumps(error))
            return HttpResponse(json.dumps(error))
