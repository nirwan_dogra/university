from django.db import models
from django.contrib.auth.models import User

class Homework(models.Model):
    title = models.CharField(max_length=255, unique=True)
    question = models.TextField(max_length=255)
    due_date = models.CharField(max_length=255)

    def get_json(self):
        return {
            'id': self.id,
            'title':self.title,
            'due_date':self.due_date
        }

    class Meta:
        db_table = 'homework'

    def __unicode__(self):
        return self.title

class HomeworkUser(models.Model):
    user = models.ForeignKey(User)
    homework = models.ForeignKey(Homework)
    status = models.IntegerField(default=0)

    def get_json(self):
        return {
            'homework_id': self.homework.id,
            'homework':self.homework.title,
            'due_date':self.homework.due_date,
        }

    class Meta:
        db_table = 'homework_user'
        index_together = unique_together = [
            ['homework', 'user']]

    def __unicode__(self):
        return str(self.id)

class Submission(models.Model):
    user = models.ForeignKey(User)
    homework = models.ForeignKey(Homework)
    answer = models.TextField(max_length=255)

    def get_json(self):
        return {
            'id': self.id,
            'username':self.user.username,
            'homework':self.homework.title,
            'homework_id':self.homework.id,
            'user_id':self.user.id,
            'answer':self.answer
        }

    class Meta:
        db_table = 'submission'

    def __unicode__(self):
        return str(self.id)

