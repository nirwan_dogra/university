from django.contrib import admin

from .models import *


class HomeworkAdmin(admin.ModelAdmin):
    pass

class SubmissionAdmin(admin.ModelAdmin):
    pass

class HomeworkUserAdmin(admin.ModelAdmin):
    pass

admin.site.register(Homework, HomeworkAdmin)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(HomeworkUser, HomeworkUserAdmin)

admin.site.site_header = 'university'
admin.site.site_title = 'university'
admin.site.site_url = 'university'
