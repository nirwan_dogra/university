import json
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth.models import User
from views import *

def get_objects(objs):
    return [obj.get_json() for obj in objs ]

# successful login redirection
def home(request):
    user = User.objects.filter(id=request.user.id)[0]
    if user.is_superuser == True:
        return render(
            request,
            'templates/dashboard/teacher.html',
            {'user': request.user}
        )
    else:
        return render(
            request,
            'templates/dashboard/student.html',
            {'user': request.user}
        )

# successful registration template

def logout_view(request):
    auth.logout(request)
    return HttpResponseRedirect("/login")

def get_users(request):
    print '------ Getting Users ------- '
    users = get_objects(UserProfile.objects.all())
    return HttpResponse(json.dumps(users))

def get_user_by_id(request, id):
    print '------ Getting User By Id ------- '
    user = get_objects(UserProfile.objects.all().filter(id=id))
    return HttpResponse(json.dumps(user))

def get_cur_user(request):
    print '------ Getting User By Id ------- '
    id = request.user.id
    user = User.objects.get(id=id)
    user_json = {
        'username':user.username,
        'id':user.id
    }
    return HttpResponse(json.dumps(user_json))

def submissions(request):
    user = User.objects.filter(id=request.user.id)[0]
    if user.is_superuser == True:
        return render(
            request,
            'templates/dashboard/submissions.html',
            {'user': request.user}
        )
    else:
        return render(
            request,
            'templates/dashboard/student.html',
            {'user': request.user}
        )

def render_homeworks(request):
    user = User.objects.filter(id=request.user.id)[0]
    if user.is_superuser == True:
        return render(
            request,
            'templates/dashboard/homeworks.html',
            {'user': request.user}
        )
    else:
        return render(
            request,
            'templates/dashboard/student.html',
            {'user': request.user}
        )

def render_homework_sub(request, homework_id):
    user = User.objects.filter(id=request.user.id)[0]
    if user.is_superuser == True:
        return render(
            request,
            'templates/dashboard/homework_submission.html',
            {'user': request.user , 'homework_id':homework_id}
        )
    else:
        return render(
            request,
            'templates/dashboard/homework_submission.html',
            {'user': request.user}
        )


urlpatterns = [
    url(r'^$', 'django.contrib.auth.views.login', {'template_name': 'templates/registration/login.html'}),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'templates/registration/login.html'}),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'templates/registration/login.html'}),
    url(r'^logout/$', login_required(logout_view)),
    url(r'^home/$', login_required(home)),

    url(r'^user/(?P<id>\d+)/$',
        login_required(get_user_by_id), name='get_user_by_id'),
    url(r'^getUsers/$', login_required(get_users), name='get_users'),

    url(r'^getCurUser/$', login_required(get_cur_user), name='get_cur_user'),

    url(r'^homework/(?P<homework_id>\d+)/submit/$', login_required(SimpleView.as_view()), name='SimpleView'),

    url(r'^user/(?P<user_id>\d+)/homeworks/$',
        login_required(SimpleView().get_homework_by_user_id), name='get_homework_by_user_id'),
    
    url(r'^homework/(?P<homework_id>\d+)/submissionss/$',
        login_required(render_homework_sub), name='render_homework_sub'),

    url(r'^homework/(?P<homework_id>\d+)/submissions/$',
        login_required(SimpleView().get_submissions_by_homework_id), name='get_submissions_by_homework_id'),
    url(r'^homework/(?P<homework_id>\d+)/user/(?P<user_id>\d+)/submissions/$',
        login_required(SimpleView().get_submissions_by_homework_id_and_user_id), name='get_submissions_by_homework_id_and_user_id'),

    url(r'^submissions$',
        login_required(submissions), name='submissions'),
    url(r'^getSubmissions$',
        login_required(SimpleView().get_submissions), name='get_submissions'),

    url(r'^homeworks$',
        login_required(render_homeworks), name='render_homeworks'),

    url(r'^getUsers$',
        login_required(SimpleView().get_users), name='get_users'),

    url(r'^getHomeworks$',
        login_required(SimpleView().get_homeworks), name='get_homeworks'),
    ]
