#!/usr/bin/env python
import pika
import urllib
import hashlib
import os
import json
import datetime
import requests
from django.conf import settings

def convert_date(s):
    try: 
        return int(datetime.datetime.strptime(s, '%Y-%m-%d').strftime("%s"))*1000
    except ValueError:
        return False

def convert_int(s):
    try:
        return int(s)
    except ValueError:
        return None

def convert_bool(s):
    try:
        s = s.lower()
        if s == 'true' or s == 'on':
            return True
        elif s == 'false' or s == 'off':
            return False
    except ValueError:
        return None

def convert_value(attribute, value):
    try:
        value = str(value)
        value_int = convert_int(value)
        value_bool = convert_bool(value)
        value_date = convert_date(value)
        if value_int or value_int == 0:
            value = value_int
        if value_bool is not None:
            value = value_bool
        if value_date:
            value = value_date
        return value
    except Exception as e:
        logger.error(traceback.format_exc())
        return value

def get_objects(objs):
    values = [obj.get_json() for obj in objs ]
    response = {}
    response['response'] = {}
    response['response']['values'] = values
    response['status'] = 200
    return response

def get_single(keys):
    res = {}
    for key in keys:
        if  len(keys[key]) == 1:
            res[key] = keys[key][0]
        else:
            res[key] = keys[key]
    return res

def get_data(request):
    try:
        data = request.body
        data = json.loads(data)
    except Exception as e:
        print str(e)
        data = request.POST
    return data

def clean_data(data):
    try:
        data = dict(data.iterlists())
        data = get_single(data)
        for d in data:
            if type(data[d]) == list:
                for i,a in enumerate(data[d]):
                    data[d][i] = convert_value(d, data[d][i])
            else:
                data[d] = convert_value(d, data[d])
        return data
    except Exception as e:
        logger.error(traceback.format_exc())
        return data

if __name__ == "__main__":
    pass